---
layout: post
title: Hakkında
permalink: /hakkinda/
---


Kişisel olarak hazırladığım ileride kendimin de faydalanacağımı düşünerek yazdığım makale,
çeviri, fikir ve diğer şeyleri içeren standar bir geliştirici sayfamdır.
